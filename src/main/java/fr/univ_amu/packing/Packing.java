package fr.univ_amu.packing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Packing implements Iterable<Tile> {

	List<Tile> listTiles;

	public Packing() {
		this.listTiles = new ArrayList<Tile>();
	}

	public int size() {
		return listTiles.size();
	}

	public void addTile(Tile t) {
		listTiles.add(t);
	}

	public Packing transform(int moveX, int moveY, int quarts) {
		Packing result = new Packing();
		for (Tile tile : this.listTiles) {
			Tile copy = tile.clone();
			copy.rotate(quarts);
			copy.translate(moveX, moveY);
			result.addTile(copy);
		}
		return result;
	}

	public static Packing concat(Packing pack1, Packing pack2) {
		Packing result = new Packing();
		for (Tile tile : pack1) {
			result.addTile(tile);
		};
		for (Tile tile : pack2) {
			result.addTile(tile);
		};
		return result;
	}

	@Override
	public Iterator<Tile> iterator() {
		return listTiles.iterator();
	}

}
