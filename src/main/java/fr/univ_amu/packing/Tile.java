package fr.univ_amu.packing;

public class Tile {

	private int cornerX;
	private int cornerY;
	private int width;
	private int height;

	public Tile(int width, int height) {
		assert (width > 0 && height > 0);
		this.width = width;
		this.height = height;
		cornerX = 0;
		cornerY = 0;
	}

	public void setCorner(int cornerX, int cornerY) {
		this.cornerX = cornerX;
		this.cornerY = cornerY;
	}

	public int getCornerX() {
		return cornerX;
	}

	public int getCornerY() {
		return cornerY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getArea() {
		return width * height;
	}

	public void flip() {
		int buffer = this.height;
		this.height = this.width;
		this.width = buffer;
	}

	public void translate(int moveX, int moveY) {
		this.cornerX = this.cornerX + moveX;
		this.cornerY = this.cornerY + moveY;
	}

	public void rotate(int quarts) {
		if (quarts % 4 == 0) { return; }
		int x = this.cornerX;
		int y = this.cornerY;
		if (quarts % 4 == 2) {
			this.cornerX = -x;
			this.cornerY = -y;
			this.translate(-this.width,-this.height);
			return;
		}
		this.flip();
		if (quarts % 4 == 1) {
			this.cornerX = -y;
			this.cornerY = x;
			this.translate(-this.width,0);
		}
		if (quarts % 4 == 3) {
			this.cornerX = y;
			this.cornerY = -x;
			this.translate(0, -this.height);
		}
		return;
	}

	@Override
	public Tile clone() {
		Tile res = new Tile(this.width,this.height);
		res.translate(this.cornerX, this.cornerY);
		return res;
	}
}
