package fr.univ_amu.packing;


public interface PairFinder<T> {
	public boolean apply(T one, T two);
}
