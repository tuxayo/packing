package fr.univ_amu.packing;

import java.util.HashMap;

public class DynProg {

	private final EllShape ground; // Le conteneur initial
	private final Tile basicTile;  // La tuile à placer
	private HashMap<EllShape, Packing> memoized;

	public DynProg(EllShape ground, Tile basicTile) {
		// À COMPLÉTER
		this.ground = ground;
		this.basicTile = basicTile;
		memoized = new HashMap<>(); //TODO : ASK IF WE CAN FORSEE THE SIZE
	}

	private class Finder implements PairFinder<EllShape.Ell> {

		// VOUS POUVEZ AJOUTER DES CHAMPS À Finder ICI
		private Packing bestSoFar;
		private int upperBound;

		public Finder(EllShape ell) {
			// À MODIFIER (PEUT-ÊTRE)
			bestSoFar = ell.greedy(basicTile);
			upperBound = ell.upperBound(basicTile);
		}

		@Override
		public boolean apply(EllShape.Ell one, EllShape.Ell two) {
			// DONE
			Packing resultPacking1 = solveShape(one.getShape());
			Packing resultPacking2 = solveShape(two.getShape());

			if(notABetterResultThanCurrentBest(resultPacking1, resultPacking2))
				return false;

			Packing mergedResult = mergeAndTranslatePackings(one, two, resultPacking1,
					resultPacking2);

			bestSoFar = mergedResult;

			// TODO: explain: For some performance reason
			// we need the same check in apply() and solveShape()
			return isBestPossiblePackingFound();
		}

		private boolean notABetterResultThanCurrentBest(Packing resultPacking1,
				Packing resultPacking2) {
			return resultPacking1.size()+resultPacking2.size() <= bestSoFar.size();
		}

		private boolean isBestPossiblePackingFound() {
			return (bestSoFar.size() == upperBound);
		}

		private Packing mergeAndTranslatePackings(EllShape.Ell one, EllShape.Ell two,
				Packing resultPacking1, Packing resultPacking2) {
			Packing resultTransformedPacking1 =
					resultPacking1.transform(
							one.getOriginX(),
							one.getOriginY(),
							one.getRotation()
							);
			Packing resultTransformedPacking2 =
					resultPacking2.transform(
							two.getOriginX(),
							two.getOriginY(),
							two.getRotation()
							);

			Packing mergedResult = Packing.concat(
												resultTransformedPacking1,
												resultTransformedPacking2
												);
			return mergedResult;
		}

		public Packing best() { return bestSoFar; }

	}

	public Packing solveShape(EllShape ell) {
		// DONE
		if(memoized.containsKey(ell)) return memoized.get(ell);
		Finder f = new Finder(ell);

		// TODO: explain: For some performance reason
		// we need the same check in apply() and solveShape()
		if (bestPossibleStillNotFound(f))
			ell.enumerateSubdivision(f);


		Packing pack = f.best();
		memoized.put(ell, pack);
		int solvedSubShapes = memoized.size();
		if (solvedSubShapes%10000 == 0) {
			System.out.println(solvedSubShapes + " Solved sub shapes");
		}

		return f.best();
	}

	private boolean bestPossibleStillNotFound(Finder f) {
		return f.bestSoFar.size() < f.upperBound;
	}

	public Packing solve() {
		return solveShape(ground);
	}

}
