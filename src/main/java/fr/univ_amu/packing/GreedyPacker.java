package fr.univ_amu.packing;

public class GreedyPacker {

	private int width;
	private int height;
	private int cornerX;
	private int cornerY;

	public GreedyPacker(int width, int height, int cornerX, int cornerY) {
		this.width = width;
		this.height = height;
		this.cornerX = cornerX;
		this.cornerY = cornerY;
	}

	public Packing greedy(Tile basicTile) {
		Packing horizontalPack = greedyHorizontal(basicTile);
		Packing verticalPack = greedyVertical(basicTile);
		return horizontalPack.size() > verticalPack.size() ? horizontalPack : verticalPack;
	}

	public Packing greedyHorizontal(Tile basicTile) {
		Packing result = new Packing();
		if (tileDoesNotFitsInEllShape(basicTile)) return result;

		addAsMuchTilesAsPossible(basicTile, result);
		return result;
	}

	public Packing greedyVertical(Tile basicTile) {
		Packing result = new Packing();
		Tile flipped = basicTile.clone();
		flipped.flip();

		if (tileDoesNotFitsInEllShape(flipped)) return result;

		addAsMuchTilesAsPossible(flipped, result);
		return result;
	}

	private void addAsMuchTilesAsPossible(Tile basicTile, Packing result) {

		int tileWidth = basicTile.getWidth();
		int maxWidth = this.getWidth() - tileWidth;
		int tileHeight = basicTile.getHeight();
		int maxHeight = this.getHeight() - tileHeight;

		for (int x = 0 ; x <= maxWidth ; x += tileWidth) {
			for (int y = 0; y <= maxHeight ; y += tileHeight) {
				if (inForbiddenArea(tileWidth, tileHeight, x, y)) break;
				Tile tileToAdd = basicTile.clone();
				tileToAdd.setCorner(x, y);
				result.addTile(tileToAdd);
			}
		}
	}

	private boolean inForbiddenArea(int tileWidth, int tileHeight, int x, int y) {
		return x + tileWidth  > cornerX && y + tileHeight > cornerY;
	}

	private boolean tileDoesNotFitsInEllShape(Tile basicTile) {
		return basicTile.getWidth() > cornerX &
			   basicTile.getHeight() > cornerY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
