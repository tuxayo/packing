package fr.univ_amu.packing;

public class EllShape {

	private int width;
	private int height;
	private int cornerX;
	private int cornerY;
	private GreedyPacker greedyPacker;

	public EllShape(int width, int height, int cornerX, int cornerY) {
		assert (width >= 0 && height >=0);
		if (width == 0 || height == 0) {
			this.width = 0;
			this.height = 0;
			this.cornerX = 0;
			this.cornerY = 0;
			return;
		};
		boolean isRect =
				width <= cornerX || height <= cornerY
				|| cornerX == 0 || cornerY == 0;
		this.width = cornerY==0 ? cornerX : width;
		this.height = cornerX==0 ? cornerY : height;
		this.cornerX = isRect ? this.width : cornerX;
		this.cornerY = isRect ? this.height : cornerY;

		this.greedyPacker = new GreedyPacker(width, height, cornerX, cornerY);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getInnerCornerX() {
		return cornerX;
	}

	public int getInnerCornerY() {
	  return cornerY;
	}

	public int getArea() {
		int area;
		int allRectangleArea = width * height;
		int forbidenZoneArea = Math.abs(width - cornerX) * Math.abs(height - cornerY);
		area = allRectangleArea - forbidenZoneArea;
		return area;
	}

	public int upperBound(Tile basicTile) {
		int maxTilesPackableInShape = getArea() / basicTile.getArea();
		return maxTilesPackableInShape;
	}
	public Packing greedy(Tile basicTile) {
		// DONE
		return greedyPacker.greedy(basicTile);
	}

	public Packing greedyVertical(Tile tile) {
		// DONE
		return greedyPacker.greedyVertical(tile);
	}

	public Packing greedyHorizontal(Tile tile) {
		// DONE
		return greedyPacker.greedyHorizontal(tile);
	}

	public boolean isRectangle() {
		return cornerX == width || cornerY == height;
	}


	public static class Ell {
	private EllShape shape;
	private int originX;
	private int originY;
	private int rotation;

	public Ell(EllShape shape, int originX, int originY,
	    int rotation) {
		this.shape = shape;
		this.originX = originX;
		this.originY = originY;
		this.rotation = rotation;
	}

	public EllShape getShape() {
		return shape;
	}
	public int getOriginX() {
		return originX;
	}
	public int getOriginY() {
		return originY;
	}
	public int getRotation() {
		return rotation;
	}

	public boolean containsSquare(int x, int y) {
		int tmp;
		x = x - originX;
		y = y - originY;
		if (rotation == 1) { tmp = x; x = y; y = -tmp-1; };
		if (rotation == 2) { x = -x-1; y = -y-1; };
		if (rotation == 3) { tmp = x; x = -y-1; y = tmp; };
		return (x >= 0 && x < shape.width
				&& y >= 0 && y < shape.height
				&& (x < shape.cornerX || y < shape.cornerY));
	}

	}

	public void enumerateSubdivision(PairFinder<Ell> f)	{
		int x1, x2, y1, y2;
		int w = width;
		int h = height;
		if (isRectangle()) {
			// no corner, horizontal cut
			for (y1 = 1; y1 <= h / 2; y1++) {
				if (f.apply(
						new Ell(new EllShape(w,y1,w,y1),0,0,0),
						new Ell(new EllShape(w,h-y1,w,h-y1),0,y1,0)
						)
					) return;
			};
			//no corner, vertical cut
			for (x1 = 1; x1 <= w / 2; x1++) {
				if (f.apply(
						new Ell(new EllShape(x1,h,x1,h),0,0,0),
						new Ell(new EllShape(w-x1,h,w-x1,h),x1,0,0)
						)
					) return;
			};
			//one corner
			for (x1 = 1; x1 < w; x1++) {
				for (y1 = 1; y1 < h; y1++) {
					if (f.apply(
							new Ell(new EllShape(x1,y1,x1,y1),0,0,0),
							new Ell(new EllShape(w,h,w-x1,h-y1),w,h,2)
							)
						) return;
				}
			};
			// two corners, vertically
			for (x1 = 1; x1 < w; x1++) {
				for (x2 = x1+1; x2 < w; x2++) {
					for (y1 = 1; y1 <= h/2; y1++) {
						if (f.apply (
								new Ell(new EllShape(h,x2,h-y1,x1),0,h,3),
								new Ell(new EllShape(h,w-x1,y1,w-x2),w,0,1)
								)
							) return;
					}
				}
			};
			// two corners, horizontally
			for (x1 = 1; x1 <= w/2; x1++) {
				for (y1=1; y1 < h; y1++) {
					for (y2=y1+1; y2 < h; y2++) {
						if (f.apply(
								new Ell(new EllShape(y2,w,y1,w-x1),w,0,1),
								new Ell(new EllShape(h-y1,w,h-y2,x1),0,h,3)
								)
							) return;
					}
				}
			};
		} else {
			// in top left part
			for (x1 = 1; x1 < cornerX; x1++) {
				for (y1 = cornerY + 1; y1 < h; y1++) {
					if (f.apply(
							new Ell(new EllShape(w-x1,y1,cornerX-x1,cornerY),x1,0,0),
							new Ell(new EllShape(h,cornerX,h-y1,x1),0,h,3)
							)
						) return;
					if (f.apply(
							new Ell(new EllShape(w,y1,x1,cornerY),0,0,0),
							new Ell(new EllShape(cornerX,h-cornerY,cornerX-x1,height-y1),
											cornerX,h,2)
							)
						) return;
				}
			};
			// in bottom right part
			for (x1 = cornerX+1; x1 < w; x1++) {
				for (y1 = 1; y1 < cornerY; y1++) {
					if (f.apply(
							new Ell(new EllShape(x1,h,cornerX,y1),0,0,0),
							new Ell(new EllShape(w-cornerX,cornerY,w-x1,cornerY-y1),
											w,cornerY,2)
							)
						) return;
					if (f.apply(
							new Ell(new EllShape(cornerY,w,y1,w-x1),w,0,1),
							new Ell(new EllShape(x1,h-y1,cornerX,cornerY-y1),0,y1,0)
							)
						) return;
				}
			};
			// in bottom left part
			for (x1 = 1; x1 < cornerX; x1++) {
				for (y1 = 1; y1 < cornerY; y1++) {
					if (f.apply(
							new Ell(new EllShape(w,h,x1,y1),0,0,0),
							new Ell(new EllShape(w-x1,h-y1,cornerX-x1,cornerY-y1),x1,y1,0)
							)
						) return;
					if (f.apply(
							new Ell(new EllShape(cornerY,w,y1,w-x1),w,0,1),
							new Ell(new EllShape(h-y1,cornerX,h-cornerY,x1),0,h,3)
							)
						) return;
					if (f.apply(
							new Ell(new EllShape(cornerY,w-x1,y1,w-cornerX),w,0,1),
							new Ell(new EllShape(h,cornerX,h-y1,x1),0,h,3)
							)
						) return;
				}
			};
			// on cornerY line
			for (x1=1;x1 < cornerX;x1++) {
				if (f.apply(
						new Ell(new EllShape(w-x1,cornerY,w-x1,cornerY),x1,0,0),
						new Ell(new EllShape(h,cornerX,h-cornerY,x1),0,h,3)
						)
					) return;
				if (f.apply(
						new Ell(new EllShape(w,h,x1,cornerY),0,0,0),
						new Ell(new EllShape(cornerX-x1,h-cornerY,cornerX-x1,h-cornerY),
										x1,cornerY,0)
						)
					) return;
				if (f.apply(
						new Ell(new EllShape(x1,h,x1,h),0,0,0),
						new Ell(new EllShape(w-x1,h,cornerX-x1,cornerY),x1,0,0)
						)
					) return;
			};
			// on cornerX line
			for (y1=1;y1<cornerY;y1++) {
				if (f.apply(
						new Ell(new EllShape(w,h,cornerX,y1),0,0,0),
						new Ell(new EllShape(w-cornerX,cornerY-y1,w-cornerX,cornerY-y1),
										cornerX,y1,0)
						)
					) return;
				if (f.apply(
						new Ell(new EllShape(cornerY,w,y1,w-cornerX),w,0,1), // 17
						new Ell(new EllShape(cornerX,h-y1,cornerX,h-y1),0,y1,0)
						)
					) return;
				if (f.apply(
						new Ell(new EllShape(w,y1,w,y1),0,0,0),
						new Ell(new EllShape(w,h-y1,cornerX,cornerY-y1),0,y1,0)
						)
					) return;
			};
			// horizontal cuts
			for (y1=cornerY;y1 < h; y1++) {
				if (f.apply(
						new Ell(new EllShape(w,y1,cornerX,cornerY),0,0,0),
						new Ell(new EllShape(cornerX,h-y1,cornerX,h-y1),0,y1,0)
						)
					) return;
			};
			// vertical cuts
			for (x1=cornerX; x1 < w; x1++) {
				if (f.apply(
						new Ell(new EllShape(x1,h,cornerX,cornerY),0,0,0),
						new Ell(new EllShape(w-x1,cornerY,w-x1,cornerY),x1,0,0)
						)
					) return;
			}
		}

	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cornerX;
		result = prime * result + cornerY;
		result = prime * result + height;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EllShape other = (EllShape) obj;
		if (cornerX != other.cornerX)
			return false;
		if (cornerY != other.cornerY)
			return false;
		if (height != other.height)
			return false;
		if (width != other.width)
			return false;
		return true;
	}
}
