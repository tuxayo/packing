package fr.univ_amu.packing;

import java.util.ArrayList;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.lang.Math;

@SuppressWarnings("serial")
public class DrawArea extends JPanel {

	private final int ellMargin = 0;
	private final int tileMargin = 2;
	private int side;
	private ArrayList<EllShape.Ell> ells;
	private ArrayList<Tile> tiles;

	public DrawArea(int width, int height) {
		ells = new ArrayList<EllShape.Ell>();
		tiles = new ArrayList<Tile>();
		resize(width,height);
		setPreferredSize(new Dimension(1400, 800));
	}

	@Override
	public void resize(int width, int height) {
		this.side =
			Math.max(
				20,
				Math.min(400 / Math.max(1,width),
						400 / Math.max(1,height))
			);

	}

	public void addTile(Tile t) {
		tiles.add(t);
	}

	public void addEll(EllShape.Ell ell) {
		ells.add(ell);
	}

	public void clear() {
		tiles = new ArrayList<Tile>();
		ells = new ArrayList<EllShape.Ell>();
	}

	private void drawEll(Graphics g, EllShape.Ell ell) {
		int[] x = new int[6];
		int[] y = new int[6];
		EllShape shape = ell.getShape();
		x[0] = 0; y[0] = 0;
		x[1] = shape.getWidth(); y[1] = 0;
		x[2] = shape.getWidth(); y[2] = shape.getInnerCornerY();
		x[3] = shape.getInnerCornerX(); y[3] = shape.getInnerCornerY();
		x[4] = shape.getInnerCornerX(); y[4] = shape.getHeight();
		x[5] = 0; y[5] = shape.getHeight();
		int i;
		int tmp;
		for (i = 0; i < 6; i++) {
			// add margin
			if (x[i] == 0) x[i] = ellMargin;
			else x[i] = side * x[i] - ellMargin;
			if (y[i] == 0) y[i] = ellMargin;
			else y[i] = side * y[i] - ellMargin;
			//rotate
			tmp = x[i];
			switch (ell.getRotation()) {
				case 1 : x[i] = -y[i]; y[i] = tmp; break;
				case 2 : x[i] = -x[i]; y[i] = -y[i]; break;
				case 3 : x[i] = y[i]; y[i] = -tmp; break;
				default : break;
			};
			// translate
			x[i] = x[i] + ell.getOriginX();
			y[i] = y[i] + ell.getOriginY();
		}
		g.fillPolygon(x,y,6);
	}

	private void drawTile(Graphics g, Tile tile) {
		if (tile.getHeight() > tile.getWidth())
			g.setColor(new Color(0.2f,0.2f,0.9f));
		else g.setColor(new Color(0.9f,0.2f,0.2f));
		g.fillRect(
			side * tile.getCornerX() + tileMargin,
			side * tile.getCornerY() + tileMargin,
			side * tile.getWidth() - 2 * tileMargin,
			side * tile.getHeight()- 2 * tileMargin
		);
		if (tile.getHeight() > tile.getWidth())
			g.setColor(new Color(0.5f, 0.5f, 0.9f));
		else g.setColor(new Color(0.9f, 0.5f, 0.5f));

		int innerMargin = Math.max(2,side / 20);
		for (int w = 0; w < tile.getWidth(); w++) {
			for (int h = 0; h < tile.getHeight(); h++) {
				g.fillRect (
				  side * (tile.getCornerX() + w) + 4 * innerMargin,
				  side * (tile.getCornerY() + h) + 4 * innerMargin,
				  side - 8 * innerMargin,
				  side - 8 * innerMargin
				);
			};
		};
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(new Color(0.1f,0.1f,0.1f));
		for (EllShape.Ell ell : ells) {
			drawEll(g,ell);
		};
		for (Tile tile : tiles) {
			drawTile(g,tile);
		}
	}
}
