package fr.univ_amu.packing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JFrame;


public class Main {

	private static Scanner sc;
	private final static JFrame window = new JFrame("Solution");
	private final static DrawArea pict = new DrawArea(13,13);
	private final static MouseHandler activeMouseHandler = new MouseHandler ();
	private final static MouseAdapter emptyHandler = new MouseAdapter() {};

	public static void readAndDraw() {
		long startTime = System.currentTimeMillis();
		pict.removeMouseListener(activeMouseHandler);
		try {
			pict.addMouseListener(emptyHandler);
			int width = sc.nextInt();
			int height = sc.nextInt();
			int w = sc.nextInt();
			int h = sc.nextInt();
			EllShape ground = new EllShape(width,height,width,height); // using data from file
//			EllShape ground = new EllShape(12, 12, 3, 2); // for test purposes
			Tile basicTile = new Tile(w,h); // using data from file
//			Tile basicTile = new Tile(3,4); // for test purposes
			DynProg dyn = new DynProg(ground,basicTile);
			Packing sol = dyn.solve();

			pict.clear();
			pict.resize(width,height);
			pict.addEll(new EllShape.Ell(ground,0,0,0));
			for (Tile tile : sol) {
				pict.addTile(tile);
			};
			pict.repaint();
			pict.removeMouseListener(emptyHandler);
			pict.addMouseListener(activeMouseHandler);

			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println("Time for this packing: " + totalTime/1000.0 + " seconds");
		} catch (NoSuchElementException exc) {
			sc.close();
		}
	}


	private static class MouseHandler extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent ev) {
			readAndDraw();
		}
	}

	public static void main(String[] args) throws FileNotFoundException {

		sc = new Scanner(new File(args[0]));

		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.getContentPane().add(pict);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setVisible(true);

		pict.addMouseListener(activeMouseHandler);
		readAndDraw();
	}
}
