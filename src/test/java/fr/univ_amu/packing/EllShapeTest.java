package fr.univ_amu.packing;

import static org.junit.Assert.*;

import org.junit.Test;

public class EllShapeTest {

	@Test
	public void testCoord() {
		EllShape ell = new EllShape(5, 4, 3, 2);
		assertTrue(ell.getWidth() == 5);
		assertTrue(ell.getHeight() == 4);
		assertTrue(ell.getInnerCornerX() == 3);
		assertTrue(ell.getInnerCornerY() == 2);
		ell = new EllShape(8, 9, 0, 6);
		assertTrue(ell.getWidth() == 8);
		assertTrue(ell.getHeight() == 6);
		assertTrue(ell.getInnerCornerX() == 8);
		assertTrue(ell.getInnerCornerY() == 6);
	}

	@Test
	public void testArea() {
		EllShape ell = new EllShape(7, 9, 3, 4);
		assertTrue(ell.getArea() == 43);
		ell = new EllShape(11, 13, 7, 8);
		assertTrue(ell.getArea() == 123);
		ell = new EllShape(12, 8, 0, 5);
		assertTrue(ell.getArea() == 60);
	}

	@Test
	public void testIsRectangle() {
		EllShape ell = new EllShape(7, 6, 5, 4);
		assertFalse(ell.isRectangle());
		ell = new EllShape(11, 10, 11, 8);
		assertTrue(ell.isRectangle());
		ell = new EllShape(8, 6, 8, 6);
		assertTrue(ell.isRectangle());
		ell = new EllShape(11, 9, 8, 0);
		assertTrue(ell.isRectangle());
	}

	@Test
	public void testUpperBound() {
		EllShape ell = new EllShape(7, 9, 3, 4);
		assertTrue(ell.upperBound(new Tile(2, 5)) == 4);
		ell = new EllShape(10, 8, 3, 4);
		assertTrue(ell.upperBound(new Tile(1, 5)) == 10);
	}

	@Test
	public void testGreedyHorizontal() {
		EllShape ell = new EllShape(7, 9, 5, 4);
		Packing hor = ell.greedyHorizontal(new Tile(2, 4));
		assertEquals(5, hor.size());
		ell = new EllShape(12, 8, 6, 4);
		hor = ell.greedyHorizontal(new Tile(3, 2));
		assertTrue(hor.size() == 12);
		ell = new EllShape(12, 12, 3, 2);
		hor = ell.greedyHorizontal(new Tile(4, 3));
		assertTrue(hor.size() == 0);
	}

	@Test
	public void testGreedyHorizontal2() {
		EllShape ell = new EllShape(12, 12, 3, 2);
		Packing pack = ell.greedyHorizontal(new Tile(3, 4));
		assertEquals(3, pack.size());
	}

	@Test
	public void testGreedyVertical() {
		EllShape ell = new EllShape(7, 9, 5, 4);
		Packing hor = ell.greedyVertical(new Tile(2, 4));
		assertTrue(hor.size() == 4);
		ell = new EllShape(12, 8, 6, 4);
		hor = ell.greedyVertical(new Tile(3, 2));
		assertTrue(hor.size() == 9);
		ell = new EllShape(12, 12, 3, 2);
		hor = ell.greedyVertical(new Tile(3, 4));
		assertTrue(hor.size() == 0);
	}

	@Test
	public void testGreedyVertical2() {
		EllShape ell = new EllShape(12, 12, 3, 2);
		Packing pack = ell.greedyVertical(new Tile(3, 4));
		assertEquals(0, pack.size());
	}

	@Test
	public void testGreedy() {
		EllShape ell = new EllShape(7, 9, 5, 4);
		Packing pack = ell.greedy(new Tile(2, 4));
		assertTrue(pack.size() == 5);
		ell = new EllShape(12, 8, 6, 4);
		pack = ell.greedy(new Tile(3, 2));
		assertTrue(pack.size() == 12);
		ell = new EllShape(12, 12, 3, 2);
		pack = ell.greedy(new Tile(3, 4));
		assertEquals(3, pack.size());
	}

	@Test
	public void testGreedy2() {
		EllShape ell = new EllShape(12, 12, 3, 2);
		Packing pack = ell.greedy(new Tile(4, 3));
		assertEquals(3, pack.size());
	}

	@Test
	public void testContainsSquare0() {
		EllShape ellShape = new EllShape(4, 5, 2, 3);
		EllShape.Ell ell = new EllShape.Ell(ellShape, 10, 7, 0);
		assertTrue(ell.containsSquare(10, 7));
		assertTrue(ell.containsSquare(13, 9));
		assertTrue(ell.containsSquare(13, 9));
		assertTrue(ell.containsSquare(10, 11));
		assertTrue(ell.containsSquare(11, 11));
		assertFalse(ell.containsSquare(10, 6));
		assertFalse(ell.containsSquare(9, 7));
		assertFalse(ell.containsSquare(12, 10));
		assertFalse(ell.containsSquare(14, 9));
		assertFalse(ell.containsSquare(10, 12));
	}

	@Test
	public void testContainsSquare1() {
		EllShape ellShape = new EllShape(4, 5, 2, 3);
		EllShape.Ell ell = new EllShape.Ell(ellShape, 10, 7, 1);
		assertTrue(ell.containsSquare(9, 7));
		assertTrue(ell.containsSquare(9, 10));
		assertTrue(ell.containsSquare(7, 10));
		assertTrue(ell.containsSquare(5, 8));
		assertTrue(ell.containsSquare(5, 7));
		assertFalse(ell.containsSquare(10, 7));
		assertFalse(ell.containsSquare(9, 6));
		assertFalse(ell.containsSquare(6, 9));
		assertFalse(ell.containsSquare(9, 11));
		assertFalse(ell.containsSquare(4, 7));
	}

	@Test
	public void testContainsSquare2() {
		EllShape ellShape = new EllShape(4, 5, 2, 3);
		EllShape.Ell ell = new EllShape.Ell(ellShape, 10, 7, 2);
		assertTrue(ell.containsSquare(9, 6));
		assertTrue(ell.containsSquare(6, 6));
		assertTrue(ell.containsSquare(6, 4));
		assertTrue(ell.containsSquare(8, 2));
		assertTrue(ell.containsSquare(9, 2));
		assertFalse(ell.containsSquare(10, 6));
		assertFalse(ell.containsSquare(9, 7));
		assertFalse(ell.containsSquare(7, 3));
		assertFalse(ell.containsSquare(5, 6));
		assertFalse(ell.containsSquare(9, 1));
	}

	@Test
	public void testContainsSquare3() {
		EllShape ellShape = new EllShape(4, 5, 2, 3);
		EllShape.Ell ell = new EllShape.Ell(ellShape, 10, 7, 3);
		assertTrue(ell.containsSquare(10, 6));
		assertTrue(ell.containsSquare(10, 3));
		assertTrue(ell.containsSquare(12, 3));
		assertTrue(ell.containsSquare(14, 6));
		assertTrue(ell.containsSquare(14, 5));
		assertFalse(ell.containsSquare(10, 7));
		assertFalse(ell.containsSquare(9, 6));
		assertFalse(ell.containsSquare(10, 2));
		assertFalse(ell.containsSquare(15, 6));
		assertFalse(ell.containsSquare(13, 4));
	}

}
