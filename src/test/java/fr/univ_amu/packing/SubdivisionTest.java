package fr.univ_amu.packing;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SubdivisionTest {

	private int count;
	private EllShape ell;

	private class Finder implements PairFinder<EllShape.Ell> {

		@Override
		public boolean apply(EllShape.Ell one, EllShape.Ell two) {
			int ar1, ar2, ar;
			ar = ell.getArea();
			ar1 = one.getShape().getArea();
			ar2 = two.getShape().getArea();
			assertTrue(ar == ar1 + ar2);
			assertTrue(ar1 > 0);
			assertTrue(ar2 > 0);
			for (int i = 0; i < ell.getWidth(); i++) {
				for (int j = 0; j < ell.getHeight(); j++) {
					assertTrue(
						(i >= ell.getInnerCornerX()
						 && j >= ell.getInnerCornerY()
						)
						|| one.containsSquare(i,j)
						|| two.containsSquare(i,j)
					);
				}
			}
			assertTrue(true);
			count++;
			return false;
		}
	}

	@Test
	public void testEllSubdiv() {
		ell = new EllShape(4,4,2,2);
		count = 0;
		Finder find = new Finder();
		ell.enumerateSubdivision(find);
		assertTrue(count==17);
	}

	@Test
	public void testBigEllSubdiv() {
		ell = new EllShape(6,5,4,3);
		count = 0;
		Finder find = new Finder();
		ell.enumerateSubdivision(find);
		assertTrue(count==47);
	}

	@Test
	public void testBigBigEllSubdiv() {
		ell = new EllShape(17,15,11,9);
		count = 0;
		Finder find = new Finder();
		ell.enumerateSubdivision(find);
	}


	@Test
	public void testRectSubdiv() {
		ell = new EllShape(3,3,3,3);
		count = 0;
		Finder find = new Finder();
		ell.enumerateSubdivision(find);
		assertTrue(count==8);
	}

	@Test
	public void testBigRectSubdiv() {
		ell = new EllShape(3,5,3,5);
		count = 0;
		Finder find = new Finder();
		ell.enumerateSubdivision(find);
		assertTrue(count==19);
	}

	@Test
	public void testBigBigRectSubdiv() {
		ell = new EllShape(13,15,13,15);
		count = 0;
		Finder find = new Finder();
		ell.enumerateSubdivision(find);
	}
}
